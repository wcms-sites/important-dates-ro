core = 7.x
api = 2

; field_collection_table
projects[field_collection_table][type] = "module"
projects[field_collection_table][download][type] = "git"
projects[field_collection_table][download][url] = "https://git.uwaterloo.ca/drupal-org/field_collection_table.git"
projects[field_collection_table][download][tag] = "7.x-1.0-beta5"
projects[field_collection_table][subdir] = ""

; field_collection_fieldset
projects[field_collection_fieldset][type] = "module"
projects[field_collection_fieldset][download][type] = "git"
projects[field_collection_fieldset][download][url] = "https://git.uwaterloo.ca/drupal-org/field_collection_fieldset.git"
projects[field_collection_fieldset][download][tag] = "7.x-2.6"
projects[field_collection_fieldset][subdir] = ""

; boolean_formatter
projects[boolean_formatter][type] = "module"
projects[boolean_formatter][download][type] = "git"
projects[boolean_formatter][download][url] = "https://git.uwaterloo.ca/drupal-org/boolean_formatter.git"
projects[boolean_formatter][download][tag] = "7.x-1.3"
projects[boolean_formatter][subdir] = ""

; restws
; Base: 7.x-2.4
; Patch 226c8adc: Resovles Specifying the resource format via a URL extension (like "node/1.json") no longer works in Drupal 7.37 http://www.drupal.org/node/2484829#comment-10452911
projects[restws][type] = "module"
projects[restws][download][type] = "git"
projects[restws][download][url] = "https://git.uwaterloo.ca/drupal-org/restws.git"
projects[restws][download][tag] = "7.x-2.6"
projects[restws][subdir] = ""

; workbench_moderation
projects[workbench_moderation][type] = "module"
projects[workbench_moderation][download][type] = "git"
projects[workbench_moderation][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench_moderation.git"
projects[workbench_moderation][download][tag] = "7.x-1.4-uw_wcms4"
projects[workbench_moderation][subdir] = ""

; uw_ct_important_dates_by_yr
projects[uw_ct_important_dates_by_yr][type] = "module"
projects[uw_ct_important_dates_by_yr][download][type] = "git"
projects[uw_ct_important_dates_by_yr][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_important_dates_by_yr.git"
projects[uw_ct_important_dates_by_yr][download][tag] = "7.x-1.8"
projects[uw_ct_important_dates_by_yr][subdir] = ""

; uw_open_data_important_dates
projects[uw_open_data_important_dates][type] = "module"
projects[uw_open_data_important_dates][download][type] = "git"
projects[uw_open_data_important_dates][download][url] = "https://git.uwaterloo.ca/wcms/uw_open_data_important_dates.git"
projects[uw_open_data_important_dates][download][tag] = "7.x-1.2"
projects[uw_open_data_important_dates][subdir] = ""

; uw_open_data_important_dates
projects[uw_open_data_important_dates_preview][type] = "module"
projects[uw_open_data_important_dates_preview][download][type] = "git"
projects[uw_open_data_important_dates_preview][download][url] = "https://git.uwaterloo.ca/wcms/uw_open_data_important_dates_preview.git"
projects[uw_open_data_important_dates_preview][download][tag] = "7.x-1.0"
projects[uw_open_data_important_dates_preview][subdir] = ""
